const firebase = {
  apiKey: 'AIzaSyDWBrJzZ4J1mdh_iQddsDTHU3T-1BlMf6k',
  authDomain: 'firecha-56402.firebaseapp.com',
  databaseURL: 'https://firecha-56402.firebaseio.com/',
  projectId: 'firecha-56402',
  storageBucket: 'gs://firecha-56402.appspot.com',
  messagingSenderId: '<SENDER_ID>',
};

export {
  firebase,
};
