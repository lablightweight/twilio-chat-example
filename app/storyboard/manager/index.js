import React from 'react';
import {
  ScrollView,
} from 'react-native';

import {
  DrawerNavigator,
  DrawerItems,
  SafeAreaView,
} from 'react-navigation';

import ChatContainer from './chat/';
import UserContainer from './user/UserContainer';

const DrawerContent = (props) => (
  <ScrollView>
    <SafeAreaView style={{ flex: 1, paddingTop: 50 }} forceInset={{ top: 'always', horizontal: 'never' }} >
      <UserContainer />
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
);

const Management = DrawerNavigator({
  Chat: { screen: ChatContainer },
}, {
  contentComponent: DrawerContent,
});

export default Management;
