import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  FlatList,
  TextInput,
} from 'react-native';

import Button from 'react-native-button';
import { ChatForm as Styles, StyleConst } from 'app-style';

export default class ChatScreen extends Component {
  componentDidMount() {
    // will initialize the chat here
    if (this.props.onInit) {
      this.props.onInit();
    }
  }

  render() {
    return (
      <View
        style={{ flex: 1, backgroundColor: '#FFFFFF' }}
      >
        <View style={{ flex: 1 }} >
          <FlatList
            ref={(ref) => {
              this.listRef = ref;
            }}
            style={{ flex: 1 }}
            initialNumToRender={10}
            data={[]}
            renderItem={() => {
            }}
            keyExtractor={() => 1}
            extraData={1}
            // onRefresh={this.onRefresh}
            // refreshing={this.refreshing}
            windowSize={11}
          />
        </View>
        <View style={Styles.form} >
          <View style={Styles.row} >
            <View style={Styles.col} >
              <TextInput
                value={''}
                autoCorrect={false}
                autoCapitalize={'none'}
                onChangeText={(value) => {
                }}
                style={Styles.inputText}
                underlineColorAndroid={'transparent'}
              />
            </View>
            <View style={Styles.colFixLast} >
              <Button>
                {StyleConst.IconSend}
              </Button>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

ChatScreen.propTypes = {
  onInit: PropTypes.func,
  error: PropTypes.string,
};

ChatScreen.defaultProps = {
  onInit: null,
  error: null,
};
