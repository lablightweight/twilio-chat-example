import {
  StackNavigator,
} from 'react-navigation';

import ChatContainer from './ChatContainer';

const Manager = StackNavigator({
  ChatScreen: {
    screen: ChatContainer,
    navigationOptions: {
      // header: null,
    },
  },
});

export default Manager;
