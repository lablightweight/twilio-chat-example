import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Authentication } from 'app-redux';

import ChatScreen from './ChatScreen';

const { onLogin, onRegister } = Authentication;

const Container = (props) => {
  const error = props.error.error;
  return (
    <ChatScreen />
  );
}

const mapStateToProps = state => ({
  error: state.auth,
});

const mapDispatchToProps = dispatch => ({
  onLogin: onLogin(dispatch),
  onRegister: onRegister(dispatch),
});


Container.propTypes = {
  onInit: PropTypes.func,
  error: PropTypes.object,
};

Container.defaultProps = {
  onInit: null,
  error: null,
};


export default connect(mapStateToProps, mapDispatchToProps)(Container);
