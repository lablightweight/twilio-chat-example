import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Authentication } from 'app-redux';

import Home from './Home';

const { onStart } = Authentication;

class Container extends Component {
  componentDidMount() {
    if (this.props.onStart) {
      this.props.onStart();
    }
  }

  render() {
    return (
      <Home
        logged={!this.props.logged}
      />
    );
  }
}

const mapStateToProps = state => ({
  logged: !state.auth.user,
});

const mapDispatchToProps = dispatch => ({
  onStart: onStart(dispatch),
});

Container.propTypes = {
  onStart: PropTypes.func,
  logged: PropTypes.bool,
};

Container.defaultProps = {
  onStart: null,
  logged: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
