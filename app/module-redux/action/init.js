
import * as firebase from 'firebase';
import { FirebaseService, ChatService } from 'app-service';

export const onInit = () => {
  return (dispatch) => {
    /*
     FIREBASE_REF_MESSAGES.limitToLast(FIREBASE_REF_MESSAGES_LIMIT).on('value', (snapshot) => {
     dispatch(loadMessagesSuccess(snapshot.val()))
     }, (errorObject) => {
     dispatch(loadMessagesError(errorObject.message))
     })
     */

    if (ChatService.app.channel) {

      //sdispatch(sess)
      const channel = ChatService.app.channel;

      console.log('twilio chat loading messages');

      channel.getMessages(10)
        .then((list) => {
          console.log('twilio messages from server');
          console.log(list);
          // alert(list.length);
        })
        .catch((err) => {
          alert(err.toString());
        });

      const messages = {
        data: [
          {
            text: 'hi',
            user: {
              email: 'nice@gmail.com'
            },
            createdAt: Date.now() - 60 * 1000,
          }
        ]
      };
      ChatService.app.channel.onMessageAdded = message => {
        // dispatch(updateMessage(message.body)

        console.log(message);

        dispatch(chatNewMessageAdded({
          text: message.body,
          user: {
            email: message.author,
          },
          me: ChatService.app.client.userInfo.identity === message.author,
          createdAt: Date.parse(message.timestamp.toUTCString()),
        }))
      }


      dispatch(loadMessagesSuccess(messages));
    }


  }
}